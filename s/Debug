;
; Copyright (c) 2012, RISC OS Open Ltd
; Copyright (c) 2012, Adrian Lees
; All rights reserved.
;
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:
;     * Redistributions of source code must retain the above copyright
;       notice, this list of conditions and the following disclaimer.
;     * Redistributions in binary form must reproduce the above copyright
;       notice, this list of conditions and the following disclaimer in the
;       documentation and/or other materials provided with the distribution.
;     * Neither the name of RISC OS Open Ltd nor the names of its contributors
;       may be used to endorse or promote products derived from this software
;       without specific prior written permission.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
; POSSIBILITY OF SUCH DAMAGE.
;
; With many thanks to Broadcom Europe Ltd for releasing the source code to
; its Linux drivers, thus making this port possible.
;

                AREA    |ARM$$code|, CODE, READONLY, PIC

                GET     hdr.BCM2835

 [ Debug
                IMPORT  HAL_UARTLineStatus
                IMPORT  HAL_UARTTransmitByte
                IMPORT  HAL_UARTReceiveByte

                EXPORT  HAL_DebugTX
                EXPORT  HAL_DebugRX

                ; in a1 = char
HAL_DebugTX     STMFD   sp!,{a1,lr}
busy            MOV     a1, #0
                BL      HAL_UARTLineStatus
                TST     a1, #&20
                BEQ     busy
                LDMFD   sp!, {a2,lr}
                MOV     a1, #0
                B       HAL_UARTTransmitByte

                ; out a1 = char if there or -1
HAL_DebugRX     stmfd   sp!, {a2,lr}
                mov     a2, sp
                sub     sp,sp,#4
                bl      HAL_UARTReceiveByte
                add     sp,sp,#4
                ldr     a2, [sp]
                tst     a2, #1
                mvneq   a1, #0                  ; -1 exit if no character
                ldmfd   sp!, {a2,pc}
 ]

                END
